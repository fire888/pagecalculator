 /**
  |************************************************************************; 
  *  Project        : Module for free use
  *  Program name   : PageCalculator 
  *  Author         : vasilii /  porokh.vs@gmail.com
  *  Date           : 20.10.2017 
  *  Purpose        : Test task    
  |************************************************************************;
  * 
  |************************************************************************;
  * For use this module in main file print:
  * 
  *    var calculator =  new PageCalculator( arrayVal, val );    
  *
  * Where 'arrayVal' like '['1', 'a', 5, obj, 'string', ["a","b"]]',
  * 'val' like  '5', '3', '10'.
  *
  * For use functions this module in main file print:
  *
  *     calculator.getTotalItems();         : to get  total items,
  *     calculator.getTotalPages();         : to get  total pages,
  *     calculator.getItemsOnPage(n);       : to get quantity items on page 'n',
  *                                             where 'n' number of page,  
  *     calculator.getPageIndexByItem('a'); : to get number of page,
  *                                              where exist 'a'.  
  * 
  |*************************************************************************;  
  */
   
  function PageCalculator( collection, itemsPerPage ){
	  
     var collection, itemsPerPage;
   
     this.getTotalItems = function(){
	     return collection.length;  
     }

     this.getTotalPages = function(){
	     return Math.ceil(collection.length/itemsPerPage);    
     }
   
     this.getItemsOnPage = function(pageIndex){         
          val =0; 		    
          for (var i = pageIndex*itemsPerPage; i < pageIndex*itemsPerPage + itemsPerPage ; i++){
		      collection[i] ? val++ : val ; 
		  }			  
          return !collection[pageIndex*itemsPerPage] ? -1 : val;	  
     }
   
     this.getPageIndexByItem = function(itemIndex){       
	     return !collection.indexOf(itemIndex) ? -1 : Math.floor(collection.indexOf(itemIndex)/itemsPerPage);  
     }    
  }